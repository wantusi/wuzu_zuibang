import { ComputedOptions } from "vue"

const icons = require.context("./icons", true, /\.vue$/)
interface Component {
  [propertyName: string]: ComputedOptions
}

const res = icons.keys().reduce((returnVal: Component, item) => {
  const component = icons(item).default
  returnVal[component.__name] = component
  return returnVal
}, {})
export default res
