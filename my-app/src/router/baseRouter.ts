const baseRouter = [
    {
        path:"/",
        component: () => import("../views/Index/Index.vue"),
        meta:{
            title:"首页",
            nav:true
        }
    },
    {
        path:"/article",
        component: () => import("../views/Article/Index.vue"),
        meta:{
            title:"文章",
            nav:true
        }
    }
]

export default baseRouter