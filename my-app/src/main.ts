import { createApp } from "vue"
import App from "./App.vue"
import router from "./router"
import uiPlugin from "./plugin/ui.registry"
import baseComponent from "./plugin/baseComponent.registry"
import "@/style/var.less"
import "@/style/common.less"
import "@/style/theme.css"
import "./assets/font/iconfont.css"

createApp(App).use(router).use(uiPlugin).use(baseComponent).mount("#app")
