import { App } from "vue"
import BaseHeader from "../components/BaseHeader/Index.vue"
import icon from "../components/IconFont"

const install = (app: App) => {
  app.component("bs-header", BaseHeader)
  Object.keys(icon).forEach((key) => {
    app.component(key, icon[key])
  })
}

export default {
  install,
}
